//
//  ViewController.swift
//  SegueSwizzler
//
//  Created by  Виктор Борисович on 01.09.2019.
//  Copyright © 2019 MT. All rights reserved.
//

import UIKit
import ObjectiveC

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func onClick(_ sender: UIButton) {
        
    }
    
}

//@IBInspectable var identifier: String
private var AssociatedObjectHandle: UInt8 = 0

extension UIView {
    @IBInspectable var identifier: String {
        get {
            return objc_getAssociatedObject(self, &AssociatedObjectHandle) as! String
        }
        set {
            objc_setAssociatedObject(self, &AssociatedObjectHandle, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}
